#include "commands.h"
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <errno.h>
#include <assert.h>
#include <sys/epoll.h>

#include <term.h>

#define LOG_MODULE "commands"
#define LOG_ENABLE_DBG 1
#include "log.h"
#include "document.h"
#include "fdm.h"

struct cmds {
    struct fdm *fdm;
    int pty;
    struct document *doc;

    enum { STATE_GROUND, STATE_SEARCHING } state;
};

struct terminfo_key {
    const char *key;
    bool (*fun)(struct cmds *cmds, const uint8_t *buf, size_t len);

    const char *v;
    size_t len;
};

static bool
line_down(struct cmds *cmds, const uint8_t *buf, size_t len)
{
    return doc_line_down(cmds->doc);
}

static bool
line_up(struct cmds *cmds, const uint8_t *buf, size_t len)
{
    return doc_line_up(cmds->doc);
}

static bool
page_down(struct cmds *cmds, const uint8_t *buf, size_t len)
{
    return doc_page_down(cmds->doc);
}

static bool
page_up(struct cmds *cmds, const uint8_t *buf, size_t len)
{
    return doc_page_up(cmds->doc);
}

static bool
end(struct cmds *cmds, const uint8_t *buf, size_t len)
{
    return doc_end(cmds->doc);
}

static bool
beginning(struct cmds *cmds, const uint8_t *buf, size_t len)
{
    return doc_beginning(cmds->doc);
}

static bool
search_begin(struct cmds *cmds)
{
    assert(cmds->state == STATE_GROUND);
    cmds->state = STATE_SEARCHING;
    return true;
}

static bool terminfo_initialized = false;
static struct terminfo_key keys[] = {
    {.key = "kcud1", .fun = &line_down},  /* Arrow down */
    {.key = "kcuu1", .fun = &line_up},    /* Arrow up */
    {.key = "knp",   .fun = &page_down},  /* Page down */
    {.key = "kpp",   .fun = &page_up},    /* Page up */
    {.key = "kend",  .fun = &end},        /* End */
    {.key = "khome", .fun = &beginning},  /* Home */
};

static void
initialize_terminfo(void)
{
    for (size_t i = 0; i < sizeof(keys) / sizeof(keys[0]); i++) {
        keys[i].v = tigetstr(keys[i].key);
        keys[i].len = keys[i].v != NULL && keys[i].v != (const char *)-1
            ? strlen(keys[i].v) : 0;
    }
}

static bool
state_ground_handler(struct cmds *cmds, uint8_t *buf, size_t count)
{
    for (size_t i = 0; i < count; i++) {
        switch (buf[i]) {
        case 'q':
        case '\a':  /* BEL/ctrl+g */
            return false;

        case ' ':
            return doc_page_down(cmds->doc);

        case '\x0e':  /* ctrl+n */
            return doc_line_down(cmds->doc);

        case '\x10':  /* ctrl+p */
            return doc_line_up(cmds->doc);

        case '\x13':  /* ctrl+s */
        case '/':
            return search_begin(cmds);

        default:
            for (size_t j = 0; j < sizeof(keys) / sizeof(keys[0]); j++) {
                const struct terminfo_key *key = &keys[j];
                if (count - i >= key->len &&
                    memcmp(&buf[i], key->v, key->len) == 0)
                {
                    //assert(i + key->len <= count - i - key->len);
                    return key->fun(cmds, &buf[i + key->len], count - i - key->len);
                }
            }

            /* Debug - print escape sequences we don't recognize */
            char hex[16];
            snprintf(hex, sizeof(hex), "%02hhx ", buf[i]);
            write(STDOUT_FILENO, hex, strlen(hex));
            break;
        }
    }

    return true;
}

static bool
state_searching_handler(struct cmds *cmds, uint8_t *buf, size_t count)
{
    for (size_t i = 0; i < count; i++) {
        switch (buf[i]) {
        case 'q':
        case '\a':
            cmds->state = STATE_GROUND;
            break;
        }
    }

    return true;
}

static bool
fdm_pty(struct fdm *fdm, int fd, int events, void *data)
{
    struct cmds *cmds = data;

    if (!terminfo_initialized) {
        terminfo_initialized = true;
        initialize_terminfo();
    }

    uint8_t buf[65536];
    ssize_t count = read(fd, buf, sizeof(buf));
    if (count < 0) {
        if (errno == EINTR)
            return true;

        LOG_ERRNO("failed to read from PTY");
        return false;
    }

    /* Position cursor at status bar, to enable us to print unrecognized sequences */
    write(STDOUT_FILENO, "\033[99999H", 8);

    switch (cmds->state) {
    case STATE_GROUND:     return state_ground_handler(cmds, buf, count);
    case STATE_SEARCHING:  return state_searching_handler(cmds, buf, count);
    }


    if (events & EPOLLHUP)
        return false;

    return true;
}

struct cmds *
cmds_init(struct fdm *fdm, int pty_fd, struct document *doc)
{
    struct cmds *cmds = malloc(sizeof(*cmds));
    *cmds = (struct cmds) {
        .fdm = fdm,
        .pty = pty_fd,
        .doc = doc,
        .state = STATE_GROUND,
    };

    if (!fdm_add(fdm, pty_fd, EPOLLIN, &fdm_pty, cmds))
        goto err;

    return cmds;

err:
    free(cmds);
    return NULL;
}

void
cmds_destroy(struct cmds *cmds)
{
    if (cmds == NULL)
        return;

    fdm_del_no_close(cmds->fdm, cmds->pty);
    free(cmds);
}
