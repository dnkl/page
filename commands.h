#pragma once

#include "document.h"
#include "fdm.h"

struct cmds;
struct cmds *cmds_init(struct fdm *fdm, int pty_fd, struct document *doc);
void cmds_destroy(struct cmds *cmd);
