#include "document.h"
#include <stdlib.h>
#include <stdio.h>
#include <stdbool.h>
#include <string.h>
#include <wchar.h>
#include <assert.h>
#include <unistd.h>
#include <errno.h>
#include <limits.h>
#include <sys/epoll.h>
#include <sys/stat.h>

#include <tllist.h>

#define LOG_MODULE "document"
#define LOG_ENABLE_DBG 1
#include "log.h"
#include "statusbar.h"

#define min(x, y) ((x) < (y) ? (x) : (y))
#define max(x, y) ((x) > (y) ? (x) : (y))

enum cmd_type { CMD_JUMP_TO };

struct cmd {
    enum cmd_type cmd;
    union {
        struct {
            ssize_t start;
        } jump_to;
    } u;
    bool preemptable;
};

#if 1
/* A pre-formatted line, ready to be printed */
struct formatted_line {
    char *content;
    size_t sz;
    size_t len;
};
#endif

/* A parsed character, used by fetch */
struct character {
    wchar_t wc;
    struct {
        wchar_t *buf;
        size_t sz;
        size_t idx;
    } unprintables;
    bool bold;
    bool italic;
    bool bold_or_italic;
};

struct line {
    struct character *cells;
};

/* Decoded, but unparsed data */
struct decode_buffer {
    wchar_t *buf;
    size_t sz;
    size_t left;
    size_t idx;
    mbstate_t ps;
};

/* Raw data (not decoded, not parsed) */
struct raw_buffer {
    char *buf;
    size_t sz;
    size_t left;
    size_t idx;
    bool eof;
};

struct fetch {
    size_t line_idx;
    size_t lines_left;

    struct line *line;
    size_t col;

    struct raw_buffer raw;
    struct decode_buffer decode;

    bool unprintable;
};

struct document {
    struct fdm *fdm;
    struct statusbar *bar;
    int fd;
    bool use_fdm;
    size_t term_lines;
    size_t term_cols;
    bool raw_passthrough;
    bool parse_overstrikes;

    tll(struct cmd) cmd_queue;

    /* Index of currently "selected" line (top line) */
    size_t cur_line;

    /* All lines parsed so far. */
    //struct formatted_line **lines;
    struct line **lines;
    size_t line_count;
    bool eof;

    /* Fetch/formatting state */
    struct fetch fetch;
};

struct document *
doc_init(struct fdm *fdm, int fd, size_t term_lines, size_t term_cols, bool raw_passthrough,
         bool parse_overstrikes, struct statusbar *bar)
{
    struct stat st;
    if (fstat(fd, &st) < 0) {
        LOG_ERRNO("failed to stat");
        return NULL;
    }

    struct document *doc = malloc(sizeof(*doc));
    *doc = (struct document) {
        .fdm = fdm,
        .bar = bar,
        .fd = fd,
        .use_fdm = S_ISFIFO(st.st_mode) || S_ISSOCK(st.st_mode),
        .term_lines = term_lines,
        .term_cols = term_cols,
        .raw_passthrough = raw_passthrough,
        .parse_overstrikes = parse_overstrikes,
        .cmd_queue = tll_init(),
        .cur_line = (size_t)-1,
        .lines = NULL,
        .line_count = 0,
        .eof = false,
        .fetch = {
            .line_idx = 0,
            .lines_left = 0,
            .col = 0,
            .raw = {
                .buf = malloc(4096),
                .sz = 4096,
                .left = 0,
                .idx = 0,
                .eof = false,
            },
            .decode = {
                .buf = malloc(4096 * sizeof(wchar_t)),
                .sz = 4096,
                .left = 0,
                .idx = 0,
                .ps = {0},
            },
        },
    };

    doc->fetch.line = calloc(1, sizeof(*doc->fetch.line));
    doc->fetch.line->cells = calloc(doc->term_cols, sizeof(doc->fetch.line->cells[0]));

    return doc;
}

static void
cmd_free(struct cmd cmd)
{
    switch (cmd.cmd) {
    case CMD_JUMP_TO:
        break;
    }
}

static void
line_free(const struct document *doc, struct line *line)
{
    for (size_t i = 0; i < doc->term_cols; i++)
        free(line->cells[i].unprintables.buf);
    free(line->cells);
    free(line);
}

void
doc_destroy(struct document *doc)
{
    if (doc == NULL)
        return;

    if (doc->fetch.lines_left > 0 && doc->use_fdm)
        fdm_del_no_close(doc->fdm, doc->fd);

    line_free(doc, doc->fetch.line);
    free(doc->fetch.raw.buf);
    free(doc->fetch.decode.buf);

    for (size_t i = 0; i < doc->line_count; i++)
        line_free(doc, doc->lines[i]);
    free(doc->lines);

    tll_free_and_free(doc->cmd_queue, cmd_free);
    free(doc);
}

static void
line_append_n(struct formatted_line *line, const char *s, size_t len)
{
    if (line->len + len > line->sz) {
        size_t new_sz = line->sz == 0 ? 128 : line->sz * 2;
        char *new_content = realloc(line->content, new_sz);
        if (new_content == NULL)
            abort();

        line->content = new_content;
        line->sz = new_sz;
    }

    assert(line->len + len <= line->sz);
    memcpy(&line->content[line->len], s, len);
    line->len += len;
}

static void
line_append(struct formatted_line *line, const char *s)
{
    line_append_n(line, s, strlen(s));
}

static struct formatted_line *
format_line(const struct document *doc, const struct line *line)
{
    struct formatted_line *fmt_line = calloc(1, sizeof(*line));
    bool italic = false;
    bool bold = false;

    static const char *const bold_on = "\033[1;36m";
    static const char *const bold_off = "\033[22;39m";
    static const char *const italic_on = "\033[3;34m";
    static const char *const italic_off = "\033[23;39m";

    for (size_t i = 0; i < doc->term_cols; i++) {
        if (line->cells[i].bold_or_italic) {
            if (!bold && !italic) {
                line_append(fmt_line, bold_on);
                bold = true;
            }
        }

        else {
            assert(!line->cells[i].bold_or_italic);

            if (!bold && line->cells[i].bold) {
                line_append(fmt_line, bold_on);
                bold = true;
            }

            else if (bold && !line->cells[i].bold) {
                line_append(fmt_line, bold_off);
                bold = false;
            }

            if (!italic && line->cells[i].italic) {
                line_append(fmt_line, italic_on);
                italic = true;
            }

            else if (italic && !line->cells[i].italic) {
                line_append(fmt_line, italic_off);
                italic = false;
            }
        }

        const struct character *cell = &line->cells[i];
        for (size_t j = 0; j < cell->unprintables.idx; j++) {
            char mb[MB_CUR_MAX];
            size_t len = wcrtomb(mb, cell->unprintables.buf[j], &(mbstate_t){0});
            assert((ssize_t)len >= 0);
            line_append_n(fmt_line, mb, len);
        }

        char mb[MB_CUR_MAX];
        size_t len = wcrtomb(mb, cell->wc, &(mbstate_t){0});
        assert((ssize_t)len >= 0);
        line_append_n(fmt_line, mb, len);
    }

    if (bold)
        line_append(fmt_line, bold_off);

    if (italic)
        line_append(fmt_line, italic_off);

    return fmt_line;
}

static void
doc_render(struct document *doc, size_t start)
{
    assert(start < doc->line_count);
    assert(doc->line_count - start >= doc->term_lines || doc->eof);

    size_t emit_start = start;
    size_t emit_count = doc->term_lines;
    size_t phys_line = 0;

    /* BSU - Begin Synchronized Update */
    write(STDOUT_FILENO, "\033P=1s\033\\", 7);

    if (doc->cur_line == (size_t)-1) {
        /* Erase screen and home cursor */
        write(STDOUT_FILENO, "\033[H\033[2J", 7);
    }

    else {
        ssize_t scroll = start - doc->cur_line;
        if (scroll == 0)
            goto out;

        bool reverse = scroll < 0;
        ssize_t abs_scroll = reverse ? -scroll : scroll;

        /* Scroll existing content */
        char esc[32];
        snprintf(esc, sizeof(esc), "\033[%zd%c",
                 abs_scroll, reverse ? 'T' : 'S');
        write(STDOUT_FILENO, esc, strlen(esc));

        /* Position cursor at the beginning of the newly scrolled in area */
        phys_line = max(0, (ssize_t)doc->term_lines - abs_scroll);
        snprintf(esc, sizeof(esc), "\033[%zuH", !reverse ? phys_line + 1 : 0);
        write(STDOUT_FILENO, esc, strlen(esc));

        /* Adjust start and range of (new) lines to emit */
        emit_start = !reverse ? emit_start + phys_line : emit_start;
        emit_count = min((size_t)abs_scroll, doc->term_lines);
    }

    /* Emit new lines */
    for (size_t i = emit_start; i < doc->line_count && i < emit_start + emit_count; i++, phys_line++) {
        struct formatted_line *line = format_line(doc, doc->lines[i]);

        assert(line != NULL);
        write(STDOUT_FILENO, line->content, line->len);

        if (phys_line + 1 < doc->term_lines)
            write(STDOUT_FILENO, "\r\n", 2);

        free(line->content);
        free(line);
    }

    for (; phys_line < doc->term_lines; phys_line++) {
        write(STDOUT_FILENO, "\033[1;34m~\033[21;39m", 16);
        if (phys_line + 1 < doc->term_lines)
            write(STDOUT_FILENO, "\r\n", 2);
    }
out:
    doc->cur_line = start;

    statusbar_set_current_line(doc->bar, doc->cur_line);
    statusbar_render(doc->bar);

    /* ESU - End Synchronized Update */
    write(STDOUT_FILENO, "\033P=2s\033\\", 7);
}

static bool doc_fetch_continue(struct document *doc);

static bool
fdm_handler(struct fdm *fdm, int fd, int events, void *data)
{
    struct document *doc = data;

    struct raw_buffer *raw = &doc->fetch.raw;
    memmove(raw->buf, &raw->buf[raw->idx], raw->left);
    raw->idx = 0;

    if (events & EPOLLIN) {
        ssize_t ret = read(doc->fd, &raw->buf[raw->left], raw->sz - raw->left);
        if (ret < 0)
            return false;

        if (ret == 0)
            raw->eof = true;

        raw->left += ret;
        assert(raw->left <= raw->sz);
    }

    struct decode_buffer *decode = &doc->fetch.decode;
    while (raw->left > 0 && decode->left < decode->sz) {
        size_t bytes = mbrtowc(
            &decode->buf[decode->left], &raw->buf[raw->idx], raw->left,
            &decode->ps);

        if (bytes == (size_t)-2)
            break;

        else if (bytes == (size_t)-1) {
            /* Invalid sequence. Reset decoder state */
            memset(&decode->ps, 0, sizeof(decode->ps));
            raw->idx++;
            raw->left--;
        }

        else if (bytes == 0){
            decode->buf[decode->left++] = L'\0';
            raw->idx++;
            raw->left--;
        }

        else {
            decode->left++;
            raw->idx += bytes;
            raw->left -= bytes;
        }

        assert(decode->left <= decode->sz);
    }

    if (!(events & EPOLLIN) && (events & EPOLLHUP))
        raw->eof = true;

    return doc_fetch_continue(doc);
}

static bool
doc_read_raw(struct document *doc)
{
    struct decode_buffer *decode = &doc->fetch.decode;
    memmove(decode->buf, &decode->buf[decode->idx],
            decode->left * sizeof(decode->buf[0]));
    decode->idx = 0;

    if (!doc->use_fdm)
        return fdm_handler(doc->fdm, doc->fd, EPOLLIN, doc);

    return true;
}

static bool doc_execute(struct document *doc);

static void
unprintable_append_n(struct character *cell, const wchar_t *wc, size_t n)
{
    while (cell->unprintables.idx + n > cell->unprintables.sz) {
        size_t new_sz = cell->unprintables.sz == 0 ? 32 : cell->unprintables.sz * 2;
        wchar_t *new_buf = realloc(cell->unprintables.buf, new_sz * sizeof(wchar_t));
        if (new_buf == NULL)
            abort();

        cell->unprintables.buf = new_buf;
        cell->unprintables.sz = new_sz;
    }

    assert(cell->unprintables.idx + n <= cell->unprintables.sz);
    memcpy(&cell->unprintables.buf[cell->unprintables.idx], wc, n * sizeof(wchar_t));
    cell->unprintables.idx += n;
}

static void
unprintable_append(struct character *cell, wchar_t wc)
{
    unprintable_append_n(cell, &wc, 1);
}

static bool
doc_fetch_continue(struct document *doc)
{
    struct fetch *fetch = &doc->fetch;
    struct raw_buffer *raw = &fetch->raw;
    struct decode_buffer *decode = &fetch->decode;

    for (; fetch->lines_left > 0; fetch->lines_left--) {
        for (; fetch->col < doc->term_cols; ) {

            /* Make sure we can peek */
            if (decode->left < 3 && !raw->eof) {
                return doc_read_raw(doc);
            } else if (decode->left == 0) {
                if (!raw->eof)
                    return doc_read_raw(doc);

                /* No more data */
                break;
            }

            assert(decode->left > 0);
            wchar_t wc = decode->buf[decode->idx];
            int width = wcwidth(wc);

            if (width > 0 && fetch->col + width > doc->term_cols) {
                /*
                 * Character does not fit on this line.
                 *
                 * Break out to force a new line. Don't consume
                 * character since we haven't "used" it yet.
                 */
                break;
            }

            decode->idx++;
            decode->left--;

            /* Peek the two next coming characters, for overstrike parsing */
            wchar_t next_wc = decode->left >= 1 ? decode->buf[decode->idx] : 0;
            wchar_t next_next_wc = decode->left >= 2 ? decode->buf[decode->idx + 1] : 0;

            struct character *cell = &fetch->line->cells[fetch->col];

            if (wc == L'\033')
                fetch->unprintable = true;

            if (fetch->unprintable) {
                if (doc->raw_passthrough)
                    unprintable_append(cell, wc);
            }

            else if (wc == L'\n') {
                if (cell->unprintables.idx > 0) {
                    /* Make sure we "see" unprintables when formatting the final line */
                    fetch->col++;
                }
                break;
            }

            else if (wc == L'\r')
                fetch->col = 0;

            else if (wc == L'\b') {
                //assert(fetch->col > 0);
                if (fetch->col > 0)
                    fetch->col--;
            }

            else if (width > 0) {
                assert(fetch->col + width <= doc->term_cols && "unimplemented");

                if (doc->parse_overstrikes && wc == L'_' && next_wc == L'\b' && next_next_wc == L'_')
                    cell->bold_or_italic = true;

                else if (doc->parse_overstrikes && wc == L'_' && next_wc == L'\b')
                    cell->italic = true;

                else if (doc->parse_overstrikes && next_wc == L'\b' && next_next_wc == wc)
                    cell->bold = true;


                cell->wc = wc;
                fetch->col++;

                for (int i = 1; i < width; i++)
                    fetch->line->cells[fetch->col++].wc = 0;
            }

            else {
                cell->wc = L'@';
                cell->bold = true;
                fetch->col++;
            }

#if 0  /* TODO: handle e.g. \b at end of line */
            if (fetch->col == doc->term_cols - 1) {
                
            }
#endif

            if (fetch->unprintable && ((wc >= L'a' && wc <= L'z') ||
                                       (wc >= L'A' && wc <= L'Z')))
            {
                fetch->unprintable = false;
            }
        }

        /*
         * Convert our wide character line to a preformatted multibyte line
         */

        fetch->col = 0;

        assert(doc->line_count == fetch->line_idx);
        doc->lines = realloc(doc->lines, (doc->line_count + 1) * sizeof(doc->lines[0]));
        doc->lines[doc->line_count++] = fetch->line;
        statusbar_set_total_lines(doc->bar, doc->line_count);

        fetch->line_idx++;
        assert(doc->line_count == fetch->line_idx);

        doc->fetch.line = calloc(1, sizeof(*doc->fetch.line));
        doc->fetch.line->cells = calloc(doc->term_cols, sizeof(doc->fetch.line->cells[0]));

        if (decode->left == 0 && raw->eof) {
            /* No more data */
            doc->eof = true;
            doc->line_count = fetch->line_idx;
            doc->lines = realloc(
                doc->lines, doc->line_count * sizeof(doc->lines[0]));
            statusbar_set_total_lines(doc->bar, doc->line_count);
            statusbar_set_eof_seen(doc->bar);
            break;
        }
    }

    assert(fetch->lines_left == 0 || doc->eof);

    fetch->lines_left = 0;
    fetch->col = 0;

    if (doc->use_fdm)
        fdm_del_no_close(doc->fdm, doc->fd);
    return doc_execute(doc);
}

static bool
doc_fetch(struct document *doc, size_t count)
{
    if (doc->eof)
        return true;

    struct fetch *fetch = &doc->fetch;

    assert(fetch->lines_left == 0);
    fetch->lines_left = count;

    if (doc->use_fdm)
        fdm_add(doc->fdm, doc->fd, EPOLLIN, &fdm_handler, doc);

    return doc_fetch_continue(doc);
}

static bool
doc_finish_cmd(struct document *doc, struct cmd *cmd)
{
    assert(tll_length(doc->cmd_queue) >= 1);
    assert(cmd == &tll_front(doc->cmd_queue));

    cmd_free(tll_pop_front(doc->cmd_queue));

    if (tll_length(doc->cmd_queue) >= 1)
        return doc_execute(doc);

    return true;
}

static bool
doc_execute_jump_to(struct document *doc, struct cmd *cmd)
{
    assert(cmd->cmd == CMD_JUMP_TO);

    ssize_t start = max(
        0,
        min(cmd->u.jump_to.start, SSIZE_MAX - (ssize_t)doc->term_lines));
    size_t end = start + doc->term_lines;

    if (doc->eof || end <= doc->line_count) {
        start = min(
            start,
            max(0, (ssize_t)doc->line_count - (ssize_t)doc->term_lines));
        doc_render(doc, start);
        return doc_finish_cmd(doc, cmd);
    }

    else {
        size_t fetch_count = end - doc->line_count;
        return doc_fetch(doc, fetch_count);
    }
}

static bool
doc_preempt_jump_to(struct document *doc, struct cmd *cmd)
{
    if (doc->fetch.lines_left > 0 && doc->use_fdm)
        fdm_del_no_close(doc->fdm, doc->fd);
    doc->fetch.lines_left = 0;
    return true;
}

static bool
doc_execute(struct document *doc)
{
    assert(tll_length(doc->cmd_queue) >= 1);
    struct cmd *cmd = &tll_front(doc->cmd_queue);

    switch (cmd->cmd) {
    case CMD_JUMP_TO: return doc_execute_jump_to(doc, cmd);
    }

    assert(false);
    return false;
}

static bool
doc_preempt(struct document *doc)
{
    while (tll_length(doc->cmd_queue) > 0 && tll_front(doc->cmd_queue).preemptable) {
        struct cmd *cmd = &tll_front(doc->cmd_queue);
        switch (cmd->cmd) {
        case CMD_JUMP_TO: if (!doc_preempt_jump_to(doc, cmd)) return false;
        }

        tll_pop_front(doc->cmd_queue);
    }

    return true;
}

static bool
doc_enqueue_cmd(struct document *doc, struct cmd cmd)
{
    if (!doc_preempt(doc))
        return false;

    tll_push_back(doc->cmd_queue, cmd);
    if (tll_length(doc->cmd_queue) == 1)
        return doc_execute(doc);
    return true;
}

bool
doc_jump_to(struct document *doc, ssize_t lineno)
{
    doc_enqueue_cmd(
        doc, (struct cmd){
            .cmd = CMD_JUMP_TO,
            .u = {.jump_to = {.start = lineno}},
            .preemptable = true});
    return true;
}

bool
doc_page_down(struct document *doc)
{
    return doc_jump_to(doc, doc->cur_line + doc->term_lines);
}

bool
doc_page_up(struct document *doc)
{
    return doc_jump_to(doc, doc->cur_line - doc->term_lines);
}

bool
doc_line_down(struct document *doc)
{
    return doc_jump_to(doc, doc->cur_line + 1);
}

bool
doc_line_up(struct document *doc)
{
    return doc_jump_to(doc, doc->cur_line - 1);
}

bool
doc_beginning(struct document *doc)
{
    return doc_jump_to(doc, 0);
}

bool
doc_end(struct document *doc)
{
    return doc_jump_to(doc, SSIZE_MAX);
}
