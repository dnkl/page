#pragma once

#include <stdlib.h>
#include <stdbool.h>

#include "fdm.h"
#include "statusbar.h"

struct document;

struct document *doc_init(
    struct fdm *fdm, int fd, size_t term_lines, size_t term_cols,
    bool raw_passthrough, bool parse_overstrikes,
    struct statusbar *bar);
void doc_destroy(struct document *doc);

bool doc_jump_to(struct document *doc, ssize_t lineno);
bool doc_page_down(struct document *doc);
bool doc_page_up(struct document *doc);
bool doc_line_down(struct document *doc);
bool doc_line_up(struct document *doc);
bool doc_beginning(struct document *doc);
bool doc_end(struct document *doc);
