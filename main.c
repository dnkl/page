#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <wchar.h>
#include <stdint.h>
#include <stdbool.h>
#include <locale.h>
#include <termios.h>
#include <unistd.h>
#include <errno.h>
#include <assert.h>
#include <getopt.h>

#include <term.h>

#include <sys/stat.h>
#include <fcntl.h>

#define LOG_MODULE "main"
#define LOG_ENABLE_DBG 1
#include "log.h"
#include "commands.h"
#include "document.h"
#include "fdm.h"
#include "statusbar.h"
#include "version.h"

static struct termios orig_termios;

static void
print_usage(const char *prog_name)
{
    printf(
        "Usage: %s [OPTIONS] [file]....\n"
        "\n"
        "Options:\n"
        "  file                        display contents of file ('-' or omitted to read from stdin)\n"
        "  -r,--raw                    pass through escape sequences\n"
        "  -o,--overstrikes            parse overstrikes (need for man pages)\n"
        "  -v,--version                show the version number and quit\n",
        prog_name);
}

static void
terminal_raw_mode(int pty)
{
    tcgetattr(pty, &orig_termios);

    struct termios raw = orig_termios;
    raw.c_iflag &= ~(BRKINT | ICRNL | INPCK | ISTRIP | IXON);
    raw.c_oflag &= ~(OPOST);
    raw.c_cflag |= (CS8);
    raw.c_lflag &= ~(ECHO | ICANON | IEXTEN | ISIG);
    tcsetattr(pty, TCSAFLUSH, &raw);
}

static void
smcup(void)
{
    static const char *v = NULL;
    static size_t len = -1;
    if (len == (size_t)-1) {
        v = tigetstr("smcup");
        len = v != NULL && v != (const char *)-1 ? strlen(v) : 0;
    }
    write(STDOUT_FILENO, v, len);
}

static void
rmcup(void)
{
    static const char *v = NULL;
    static size_t len = -1;
    if (len == (size_t)-1) {
        v = tigetstr("rmcup");
        len = v != NULL && v != (const char *)-1 ? strlen(v) : 0;
    }
    write(STDOUT_FILENO, v, len);
}

static void
clear(void)
{
    static const char *v = NULL;
    static size_t len = -1;
    if (len == (size_t)-1) {
        v = tigetstr("clear");
        len = v != NULL && v != (const char *)-1 ? strlen(v) : 0;
    }
    write(STDOUT_FILENO, v, len);
}

static void
smkx(void)
{
    static const char *v = NULL;
    static size_t len = -1;
    if (len == (size_t)-1) {
        v = tigetstr("smkx");
        len = v != NULL && v != (const char *)-1 ? strlen(v) : 0;
    }
    write(STDOUT_FILENO, v, len);
}

static void
rmkx(void)
{
    static const char *v = NULL;
    static size_t len = -1;
    if (len == (size_t)-1) {
        v = tigetstr("rmkx");
        len = v != NULL && v != (const char *)-1 ? strlen(v) : 0;
    }
    write(STDOUT_FILENO, v, len);
}

static void
cnorm(void)
{
    static const char *v = NULL;
    static size_t len = -1;
    if (len == (size_t)-1) {
        v = tigetstr("cnorm");
        len = v != NULL && v != (const char *)-1 ? strlen(v) : 0;
    }
    write(STDOUT_FILENO, v, len);
}

static void
civis(void)
{
    static const char *v = NULL;
    static size_t len = -1;
    if (len == (size_t)-1) {
        v = tigetstr("civis");
        len = v != NULL && v != (const char *)-1 ? strlen(v) : 0;
    }
    write(STDOUT_FILENO, v, len);
}

static void
csr(int top, int bottom)
{
    static const char *v = NULL;
    if (v == NULL)
        v = tigetstr("csr");

    assert(v != NULL && v != (const char *)-1);

    char *instantiated = tiparm(v, top, bottom);
    write(STDOUT_FILENO, instantiated, strlen(instantiated));
}

static void
terminal_restore(int pty)
{
    rmkx();
    cnorm();
    rmcup();
    tcsetattr(pty, TCSAFLUSH, &orig_termios);
    tcdrain(pty);
}

int
main(int argc, char *const *argv)
{
    if (!isatty(STDOUT_FILENO)) {
        LOG_ERR("stdout is not a TTY");
        return EXIT_FAILURE;
    }

    setlocale(LC_ALL, "");

    const char *const prog_name = argv[0];
    static const struct option longopts[] = {
        {"raw",         no_argument, NULL, 'r'},
        {"overstrikes", no_argument, NULL, 'o'},
        {"version",     no_argument, NULL, 'v'},
        {"help",        no_argument, NULL, 'h'},
        {NULL,          no_argument, NULL, '\0'},
    };

    bool raw = false;
    bool overstrikes = false;

    while (true) {
        int c = getopt_long(argc, argv, "rovh", longopts, NULL);
        if (c == -1)
            break;

        switch (c) {
        case 'r':
            raw = true;
            break;

        case 'o':
            overstrikes = true;
            break;

        case 'v':
            printf("page version %s\n", PAGE_VERSION);
            return EXIT_SUCCESS;

        case 'h':
            print_usage(prog_name);
            return EXIT_SUCCESS;

        case '?':
            return EXIT_FAILURE;
        }
    }

    argc -= optind;
    argv += optind;

    log_init(LOG_FACILITY_USER, LOG_CLASS_WARNING);

    int ret = EXIT_FAILURE;
    int in_fd = -1;
    bool close_in_fd = false;

    if (argc == 0 || strcmp(argv[0], "-") == 0) {
        in_fd = STDIN_FILENO;

        if (fcntl(in_fd, F_SETFL, fcntl(in_fd, F_GETFL) | O_NONBLOCK) < 0) {
            LOG_ERRNO("failed to set non-blocking");
            goto out;
        }
    }

    else {
        in_fd = open(argv[0], O_RDONLY);
        if (in_fd == -1) {
            LOG_ERRNO("%s: failed to open", argv[0]);
            log_deinit();
            return EXIT_FAILURE;
        }
        close_in_fd = true;
    }

    assert(in_fd >= 0);

    struct fdm *fdm = NULL;
    struct statusbar *bar = NULL;
    struct cmds *cmds = NULL;
    struct document *doc = NULL;

    setupterm(NULL, STDOUT_FILENO, NULL);

    int pty = open("/dev/tty", O_RDONLY | O_NONBLOCK);
    assert(pty != -1);

    if ((fdm = fdm_init()) == NULL)
        goto out;
    if ((bar = statusbar_init(lines, lines - 1)) == NULL)
        goto out;
    if ((doc = doc_init(fdm, in_fd, lines - 1, columns, raw, overstrikes, bar)) == NULL)
        goto out;
    if ((cmds = cmds_init(fdm, pty, doc)) == NULL)
        goto out;

    if (true) {
        terminal_raw_mode(STDOUT_FILENO);
        smcup();
        clear();
        civis();
        smkx();

        csr(0, lines - 2);
    }

    doc_jump_to(doc, 0);

    while (true) {
        if (!fdm_poll(fdm))
            break;
    }

    ret = EXIT_SUCCESS;

out:
    write(STDOUT_FILENO, "\033[r", 3);
    terminal_restore(STDOUT_FILENO);

    doc_destroy(doc);
    cmds_destroy(cmds);
    statusbar_destroy(bar);
    fdm_destroy(fdm);
    close(pty);

    if (close_in_fd)
        close(in_fd);

    log_deinit();
    return ret;
}
