#include "statusbar.h"

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <stdbool.h>
#include <unistd.h>

#define min(x, y) ((x) < (y) ? (x) : (y))
#define max(x, y) ((x) > (y) ? (x) : (y))

struct statusbar {
    size_t position;
    size_t term_lines;

    size_t cur_line;
    size_t line_count;
    bool eof;
};

struct statusbar *
statusbar_init(size_t lineno, size_t term_lines)
{
    struct statusbar *bar = malloc(sizeof(*bar));
    *bar = (struct statusbar){
        .position = lineno,
        .term_lines = term_lines,
        .cur_line = 0,
        .line_count = 0,
        .eof = false,
    };

    return bar;
}

void
statusbar_destroy(struct statusbar *bar)
{
    if (bar == NULL)
        return;

    free(bar);
}

void
statusbar_set_total_lines(struct statusbar *bar, size_t count)
{
    bar->line_count = count;
}

void
statusbar_set_current_line(struct statusbar *bar, size_t lineno)
{
    bar->cur_line = lineno;
}

void
statusbar_set_eof_seen(struct statusbar *bar)
{
    bar->eof = true;
}

void
statusbar_render(struct statusbar *bar)
{
    /* Move to statusbar, reset foreground, change background color */
    char esc[64];
    snprintf(esc, sizeof(esc), "\033[%zuH\033[33;100m", bar->position);
    write(STDOUT_FILENO, esc, strlen(esc));

    /* Total number of lines (seen so far), as a string */
    char tot_lines[16];
    snprintf(tot_lines, sizeof(tot_lines), "%zu", bar->line_count);

    /* Where we are: <current line> / <total lines> */
    char location[64];
    snprintf(location, sizeof(location), "%zu/%s",
             bar->cur_line, bar->eof ? tot_lines : "-");
    write(STDOUT_FILENO, location, strlen(location));

    /* Relative position in document */
    if (bar->eof) {
        if (bar->line_count <= bar->cur_line + bar->term_lines) {
            write(STDOUT_FILENO, " (EOF)", 6);
        } else {
            char percent[16];
            snprintf(
                percent, sizeof(percent), " (%zu%%)",
                min(100,
                    100 * (bar->cur_line + bar->term_lines) / bar->line_count));
            write(STDOUT_FILENO, percent, strlen(percent));
        }
    }

    /* Clear the remainder of the line, change color back to default */
    write(STDOUT_FILENO, "\033[K\033[39;49m", 11);
}
