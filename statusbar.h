#pragma once

#include <stddef.h>

struct statusbar;

struct statusbar *statusbar_init(size_t lineno, size_t term_lines);
void statusbar_destroy(struct statusbar *bar);

void statusbar_render(struct statusbar *bar);

void statusbar_set_total_lines(struct statusbar *bar, size_t count);
void statusbar_set_current_line(struct statusbar *bar, size_t lineno);
void statusbar_set_eof_seen(struct statusbar *bar);
